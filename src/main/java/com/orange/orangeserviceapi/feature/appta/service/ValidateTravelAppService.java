package com.orange.orangeserviceapi.feature.appta.service;

import com.orange.orangeserviceapi.common.exception.OrangeServiceApiBusinessException;
import com.orange.orangeserviceapi.common.exception.OrangeServiceApiDataValidateException;
import com.orange.orangeserviceapi.common.exception.OrangeServiceApiException;
import com.orange.orangeserviceapi.common.utils.DateUtils;
import com.orange.orangeserviceapi.common.utils.RsaUtils;
import com.orange.orangeserviceapi.common.utils.ResultCode;
import com.orange.orangeserviceapi.datasource.entities.AppTravelDetail;
import com.orange.orangeserviceapi.datasource.entities.Application;
import com.orange.orangeserviceapi.datasource.entities.Beneficiary;
import com.orange.orangeserviceapi.datasource.entities.CustomerDetail;
import com.orange.orangeserviceapi.feature.appta.controller.domian.BeneficiaryRequestController;
import com.orange.orangeserviceapi.feature.appta.controller.domian.CustomerDetailRequestController;
import com.orange.orangeserviceapi.feature.appta.controller.domian.TravelAppRequestController;
import io.micrometer.core.instrument.util.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.regex.Pattern;

@Slf4j
@Service
public class ValidateTravelAppService {
	private static final String APPLICATION_TYPE = "TA";

	public Application validateSaveApplicationTravel(TravelAppRequestController applicationRequest) throws OrangeServiceApiException {

		if (StringUtils.isEmpty(applicationRequest.getPlanCode()) || applicationRequest.getPlanCode().length() > 10)
			throw new OrangeServiceApiDataValidateException(ResultCode.INVALID_PARAM,"Plan code be not empty or length more than 10.");

		Application application = new Application();
		application.setRowId(this.generateApplicationRowId());
		application.setApplicationNumber(this.generateApplicationNumber());
		application.setApplicationType(APPLICATION_TYPE);
		application.setIssueDate(new Date());
		application.setPlanCode(applicationRequest.getPlanCode());
		return application;
	}

	private String generateApplicationNumber() {
		SimpleDateFormat dateFormatApplication = new SimpleDateFormat("yyyyMMddHHmmss");
		return APPLICATION_TYPE.concat(dateFormatApplication.format(DateUtils.getCurrentDate()));
	}

	private String generateApplicationRowId() {
		return UUID.randomUUID().toString().replace("-", "");
	}
	
	private boolean validateEmail(String email) { 
	      String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\."+ 
	                  "[a-zA-Z0-9_+&*-]+)*@" + 
	                  "(?:[a-zA-Z0-9-]+\\.)+[a-z" + 
	                  "A-Z]{2,7}$"; 
	                    
	      Pattern pat = Pattern.compile(emailRegex); 
	      return pat.matcher(email).matches(); 
	} 
	
	public CustomerDetail validateSaveCustomerDetail(CustomerDetailRequestController customerDetailRequest)throws OrangeServiceApiException {
		if (StringUtils.isEmpty(customerDetailRequest.getTitle()))
			throw new OrangeServiceApiDataValidateException(ResultCode.INVALID_PARAM, "Title name be not empty.");

		if (StringUtils.isEmpty(customerDetailRequest.getName()))
			throw new OrangeServiceApiDataValidateException(ResultCode.INVALID_PARAM, "Buyer name be not empty.");

		if (StringUtils.isEmpty(customerDetailRequest.getLastName()))
			throw new OrangeServiceApiDataValidateException(ResultCode.INVALID_PARAM, "Buyer last name be not empty.");

		if (StringUtils.isEmpty(customerDetailRequest.getPhoneNumber())
				|| customerDetailRequest.getPhoneNumber().length() > 10)
			throw new OrangeServiceApiDataValidateException(ResultCode.INVALID_PARAM, "Phone number be not empty.");

		if (StringUtils.isEmpty(customerDetailRequest.getCitizenId())
				&& StringUtils.isEmpty(customerDetailRequest.getPassportNumber()))
			throw new OrangeServiceApiDataValidateException(ResultCode.INVALID_PARAM, "Citizen id be not empty.");

		if (StringUtils.isEmpty(customerDetailRequest.getEmailAddress())
				|| !validateEmail(customerDetailRequest.getEmailAddress()))
			throw new OrangeServiceApiDataValidateException(ResultCode.INVALID_PARAM, "Email be not empty.");

		if (StringUtils.isEmpty(customerDetailRequest.getAddress()))
			throw new OrangeServiceApiDataValidateException(ResultCode.INVALID_PARAM, "Address be not empty.");

		CustomerDetail customerDetail = new CustomerDetail();
		BeanUtils.copyProperties(customerDetailRequest, customerDetail);
		customerDetail.setRowId(this.generateApplicationRowId());
		customerDetail.setMobileNumberEncr(RsaUtils.encrypt(customerDetail.getPhoneNumber()));
		customerDetail.setPhoneNumber(RsaUtils.markMobileNumber(customerDetail.getPhoneNumber()));
		if (StringUtils.isNotEmpty(customerDetailRequest.getCitizenId())){
			customerDetail.setCitizenIdEncr(RsaUtils.encrypt(customerDetail.getCitizenId()));
			customerDetail.setCitizenId(RsaUtils.markCitizenId(customerDetail.getCitizenId()));
		}
		return customerDetail;
	}

	public AppTravelDetail validateBeforeSaveAppTravelDetail(TravelAppRequestController appTravelDetailRequest) throws OrangeServiceApiException {
		if ((appTravelDetailRequest.getDepartureDate() == null) || appTravelDetailRequest.getArrivalDate() == null)
			throw new OrangeServiceApiDataValidateException(ResultCode.INVALID_PARAM,"Departure date be not empty.");

		if (StringUtils.isEmpty(appTravelDetailRequest.getLocation()))
			throw new OrangeServiceApiDataValidateException(ResultCode.INVALID_PARAM,"Location be not empty.");

		AppTravelDetail appTravelDetail = new AppTravelDetail();
		appTravelDetail.setDepartureDate(appTravelDetailRequest.getDepartureDate());
		appTravelDetail.setArrivalDate(appTravelDetailRequest.getArrivalDate());
		appTravelDetail.setLocation(appTravelDetailRequest.getLocation());
		appTravelDetail.setRowId(this.generateApplicationRowId());
		return appTravelDetail;
	}
	
	public List<Beneficiary> validateSaveBeneficiary(TravelAppRequestController beneficiaryRequest,String applicationNumber) throws OrangeServiceApiException {
		List<BeneficiaryRequestController> beneficiaryListRequest = beneficiaryRequest.getBeneficiaryList();
		List<Beneficiary> beneficiaryList = new ArrayList<Beneficiary>();
		if (beneficiaryListRequest.isEmpty())
			return beneficiaryList;

		String buyerName = beneficiaryRequest.getCustomerDetail().getName()+beneficiaryRequest.getCustomerDetail().getLastName();
		for (BeneficiaryRequestController beneficiaryCache : beneficiaryListRequest){
				if (StringUtils.isEmpty(beneficiaryCache.getName()))
					throw new OrangeServiceApiDataValidateException(ResultCode.INVALID_PARAM,"Name be not empty.");

				if (StringUtils.isEmpty(beneficiaryCache.getLastName()))
					throw new OrangeServiceApiDataValidateException(ResultCode.INVALID_PARAM,"Last Name be not empty.");

				if (StringUtils.isEmpty(beneficiaryCache.getRelationship()))
					throw new OrangeServiceApiDataValidateException(ResultCode.INVALID_PARAM,"Relationship be not empty.");

				if (beneficiaryCache.getPercentage() <= 0)
					throw new OrangeServiceApiDataValidateException(ResultCode.INVALID_PARAM,"Percentage must be more than 0.");

				if (beneficiaryCache.getName().concat(beneficiaryCache.getLastName())
						.equalsIgnoreCase(buyerName))
					throw new OrangeServiceApiBusinessException(ResultCode.INVALID_BUSINESS,"Beneficiary name or last name invalid.");

				Beneficiary beneficiaryObj = new Beneficiary();
				BeanUtils.copyProperties(beneficiaryCache, beneficiaryObj);
				beneficiaryObj.setRowId(generateApplicationRowId());
				beneficiaryObj.setApplicationNumber(applicationNumber);
				beneficiaryList.add(beneficiaryObj);
		}
		return beneficiaryList;
	}
}