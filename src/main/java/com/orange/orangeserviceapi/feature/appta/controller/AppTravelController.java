package com.orange.orangeserviceapi.feature.appta.controller;

import com.orange.orangeserviceapi.feature.appta.controller.domian.TravelAppRequestController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.orange.orangeserviceapi.common.exception.OrangeServiceApiException;
import com.orange.orangeserviceapi.common.utils.GenericResponse;
import com.orange.orangeserviceapi.feature.appta.service.AppTravelService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@Api(tags = "Journey")
@RequestMapping(path = "/v1/journey")
public class AppTravelController {
	@Autowired private  AppTravelService appTravelService;

	@PostMapping
	@ApiOperation(value = "Save travel")
	public ResponseEntity<GenericResponse> saveTravelApp(@RequestHeader(name = "X-Correlation-Id", required = true) String correlationId,
														 @RequestBody TravelAppRequestController travelAppRequest)throws OrangeServiceApiException{
		GenericResponse response = this.appTravelService.saveTravelApp(travelAppRequest);
		ResponseEntity<GenericResponse> responseEntity = ResponseEntity.status(HttpStatus.CREATED).body(response);
		return  responseEntity;
	}
	
	@ApiOperation(value = "Search app")
    @GetMapping("/{app_no}")
    public ResponseEntity<GenericResponse> searchApplication(@RequestHeader(name = "X-Correlation-Id", required = true) String correlationId,
    													@PathVariable String app_no) throws OrangeServiceApiException {
    	GenericResponse response = this.appTravelService.searchApplication(app_no);
        ResponseEntity<GenericResponse> responseEntity = ResponseEntity.status(HttpStatus.OK).body(response);
        return  responseEntity;
    }
}
