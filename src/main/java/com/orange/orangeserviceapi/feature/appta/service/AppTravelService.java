package com.orange.orangeserviceapi.feature.appta.service;

import java.util.List;

import com.orange.orangeserviceapi.feature.appta.controller.domian.*;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.orange.orangeserviceapi.common.exception.OrangeServiceApiDataNotFoundException;
import com.orange.orangeserviceapi.common.exception.OrangeServiceApiException;
import com.orange.orangeserviceapi.common.utils.GenericResponse;
import com.orange.orangeserviceapi.common.utils.ResultCode;
import com.orange.orangeserviceapi.datasource.entities.AppTravelDetail;
import com.orange.orangeserviceapi.datasource.entities.Application;
import com.orange.orangeserviceapi.datasource.entities.Beneficiary;
import com.orange.orangeserviceapi.datasource.entities.CustomerDetail;
import com.orange.orangeserviceapi.datasource.repo.AppTravelDetailRepo;
import com.orange.orangeserviceapi.datasource.repo.ApplicationRepo;
import com.orange.orangeserviceapi.datasource.repo.BeneficiaryRepo;
import com.orange.orangeserviceapi.datasource.repo.CustomerDetailRepo;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class AppTravelService {
	@Autowired private ApplicationRepo applicationRepo;
	@Autowired private CustomerDetailRepo customerDetailRepo;
	@Autowired private AppTravelDetailRepo appTravelDetailRepo;
	@Autowired private BeneficiaryRepo beneficiaryRepo;
	@Autowired private ValidateTravelAppService validateAppTravelService;
	private static final String APPLICATION_TYPE = "TA";

	public GenericResponse saveTravelApp(TravelAppRequestController travelAppRequest) throws OrangeServiceApiException{
		Application application =  this.validateAppTravelService.validateSaveApplicationTravel(travelAppRequest);
		Application applicationResult = this.applicationRepo.saveAndFlush(application);
		if(applicationResult == null)
			throw new OrangeServiceApiException(ResultCode.SYSTEM_ERROR,"Save application fail.");

		CustomerDetail customerDetail = this.validateAppTravelService.validateSaveCustomerDetail(travelAppRequest.getCustomerDetail());
		customerDetail.setApplicationNumber(applicationResult.getApplicationNumber());
		CustomerDetail customerDetailResult = this.customerDetailRepo.saveAndFlush(customerDetail);
		if(customerDetailResult == null)
			throw new OrangeServiceApiException(ResultCode.SYSTEM_ERROR,"Save customer detail fail.");

		AppTravelDetail appTravelDetail = this.validateAppTravelService.validateBeforeSaveAppTravelDetail(travelAppRequest);
		appTravelDetail.setApplicationNumber(applicationResult.getApplicationNumber());
		AppTravelDetail appTravelDetailResult = this.appTravelDetailRepo.saveAndFlush(appTravelDetail);
		if(appTravelDetailResult == null)
			throw new OrangeServiceApiException(ResultCode.SYSTEM_ERROR,"Save App travel detail fail.");

		TravelAppResponseController data = new TravelAppResponseController();
		data.setApplicationNumber(applicationResult.getApplicationNumber());
		data.setPlanCode(applicationResult.getPlanCode());
		data.setDepartureDate(appTravelDetailResult.getDepartureDate());
		data.setArrivalDate(appTravelDetailResult.getArrivalDate());
		data.setLocation(appTravelDetailResult.getLocation());

		List<Beneficiary> beneficiaryList = this.validateAppTravelService.validateSaveBeneficiary(travelAppRequest,applicationResult.getApplicationNumber());
		if(beneficiaryList.isEmpty()) {
			log.info("## Save travel success with out beneficiary.");
			GenericResponse response = new GenericResponse();
			response.setStatus(ResultCode.SUCCESS);
			response.setData(data);
			return response;
		}
		List<Beneficiary> beneficiaryResultList = this.beneficiaryRepo.saveAll(beneficiaryList);
		if(beneficiaryResultList.isEmpty())
			throw new OrangeServiceApiException(ResultCode.SYSTEM_ERROR,"Save Beneficiary fail.");

		GenericResponse response = new GenericResponse();
		response.setStatus(ResultCode.SUCCESS);
		response.setData(data);
		return response;
	}
	
	public GenericResponse searchApplication(String appNo) throws OrangeServiceApiException{
		GenericResponse response = new GenericResponse();
	    Application application = this.applicationRepo.findByApplicationNumber(appNo);
	    if(application == null)
	    	throw new OrangeServiceApiDataNotFoundException(ResultCode.DATA_NOT_FOUND);
        
	    ApplicationResponseController applicationResponse = new ApplicationResponseController();
		BeanUtils.copyProperties(application, applicationResponse);
		response.setStatus(ResultCode.SUCCESS);
		response.setData(applicationResponse);
		return response;
	}
}