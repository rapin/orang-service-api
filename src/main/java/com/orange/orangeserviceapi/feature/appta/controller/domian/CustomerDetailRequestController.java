package com.orange.orangeserviceapi.feature.appta.controller.domian;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CustomerDetailRequestController {
	private String title;

	private String name;
	@JsonProperty("last_name")
	private String lastName;

	@JsonProperty("phone_number")
	private String phoneNumber;

	@JsonProperty("citizen_id")
	private String citizenId;

	@JsonProperty("passport_number")
	private String passportNumber;

	@JsonProperty("email_address")
	private String emailAddress;

	private String address;
}
