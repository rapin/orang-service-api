package com.orange.orangeserviceapi.feature.appta.controller.domian;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter @Setter
public class TravelAppResponseController {

    @JsonProperty("application_number")
    private String applicationNumber;

    @JsonProperty("plan_code")
    private String planCode;

    @JsonProperty("departure_date")
    private Date departureDate;

    @JsonProperty("arrival_date")
    private Date arrivalDate;

    private String location;
}
