package com.orange.orangeserviceapi.feature.appta.controller.domian;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;

@Getter @Setter @ToString
public class BeneficiaryRequestController {
    private String name;
    @JsonProperty("last_name")
    private String lastName;
    private int percentage;
    private String  relationship;
}
