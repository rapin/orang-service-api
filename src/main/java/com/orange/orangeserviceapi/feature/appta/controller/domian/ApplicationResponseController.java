package com.orange.orangeserviceapi.feature.appta.controller.domian;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter 
@Setter
public class ApplicationResponseController {
	@JsonProperty("application_number")
	private String applicationNumber;
	
	@JsonProperty("issue_date")
    private Date issueDate;
	
	@JsonProperty("plan_code")
    private String planCode;
	
	@JsonProperty("application_type")
    private String applicationType;
}
