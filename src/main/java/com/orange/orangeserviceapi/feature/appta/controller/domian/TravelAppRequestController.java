package com.orange.orangeserviceapi.feature.appta.controller.domian;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import java.util.Date;
import java.util.List;

@Getter @Setter
public class TravelAppRequestController {
    @JsonProperty("plan_code")
    private String planCode;

    @JsonProperty("departure_date")
    private Date departureDate;

    @JsonProperty("arrival_date")
    private Date arrivalDate;

    private String location;

    @JsonProperty("customer_info")
    private CustomerDetailRequestController customerDetail;

    @JsonProperty("beneficiary_info")
    List<BeneficiaryRequestController> beneficiaryList;
}
