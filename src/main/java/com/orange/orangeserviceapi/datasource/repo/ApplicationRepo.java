package com.orange.orangeserviceapi.datasource.repo;

import com.orange.orangeserviceapi.datasource.entities.Application;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ApplicationRepo extends JpaRepository<Application,String> {
	 public Application findByApplicationNumber(String applicationNumber);
}
