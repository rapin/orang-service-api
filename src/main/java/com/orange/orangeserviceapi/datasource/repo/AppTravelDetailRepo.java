package com.orange.orangeserviceapi.datasource.repo;

import com.orange.orangeserviceapi.datasource.entities.AppTravelDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AppTravelDetailRepo extends JpaRepository<AppTravelDetail, String> {
}
