package com.orange.orangeserviceapi.datasource.repo;

import com.orange.orangeserviceapi.datasource.entities.CustomerDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CustomerDetailRepo extends JpaRepository <CustomerDetail,String> {
}
