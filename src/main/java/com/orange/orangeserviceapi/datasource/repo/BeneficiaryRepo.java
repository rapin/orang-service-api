package com.orange.orangeserviceapi.datasource.repo;

import com.orange.orangeserviceapi.datasource.entities.Beneficiary;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BeneficiaryRepo extends JpaRepository<Beneficiary, String> {
}
