package com.orange.orangeserviceapi.datasource.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity @Getter @Setter
@Table(name = "BENEFICIARY")
public class Beneficiary {
    @Id
    @Column(name = "ROW_ID")
    private String rowId;

    @Column(name = "APPLICATION_NUMBER")
    private String applicationNumber;

    @Column(name =  "NAME")
    private String name;

    @Column(name = "LAST_NAME")
    private String lastName;

    @Column(name = "PERCENTAGE")
    private Integer percentage;

    @Column(name = "RELATIONSHIP")
    private String  relationship;
}
