package com.orange.orangeserviceapi.datasource.entities;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Getter
@Setter
@ToString
@Table(name = "APP_TRAVEL_DETAIL")
public class AppTravelDetail {
    @Id
    @Column(name = "ROW_ID")
    private String rowId;

    @Column(name = "APPLICATION_NUMBER")
    private String applicationNumber;

    @Column(name = "DEPARTURE_DATE")
    private Date departureDate;

    @Column(name = "ARRIVAL_DATE")
    private Date arrivalDate;

    @Column(name = "LOCATION")
    private String location;

}
