package com.orange.orangeserviceapi.datasource.entities;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Getter @Setter @ToString
@Entity
@Table(name = "APPLICATION")
public class Application {
	@Id
    @Column(name = "ROW_ID")
    private String rowId;

    @Column(name = "APPLICATION_NUMBER")
    private String applicationNumber;

    @Column(name = "ISSUE_DATE")
    private Date issueDate;

    @Column(name = "PLAN_CODE")
    private String planCode;

    @Column(name = "APPLICATION_TYPE")
    private String applicationType;

}
