package com.orange.orangeserviceapi.datasource.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Getter @Setter @Entity
@Table(name = "CUSTOMER_DETAIL")
public class CustomerDetail {
   
	@Id
    @Column(name = "ROW_ID")
    private String rowId;

    @Column(name = "TITLE")
    private String title;

    @Column(name = "NAME")
    private String name;

    @Column(name = "LAST_NAME")
    private String lastName;

    @Column(name = "PHONE_NUMBER")
    private String phoneNumber;

    @Column(name = "CITIZEN_ID")
    private String citizenId;

    @Column(name = "PASSPORT_NUMBER")
    private String passportNumber;

    @Column(name = "EMAIL_ADDRESS")
    private String emailAddress;

    @Column(name = "ADDRESS")
    private String address;

    @Column(name = "APPLICATION_NUMBER")
    private String  applicationNumber;
    
    @Column(name = "MOBILE_NUMBER_ENCR")
    private String  mobileNumberEncr;
    
    @Column(name = "CITIZEN_ID_ENCR")
    private String  citizenIdEncr;
}
