package com.orange.orangeserviceapi.common.handler;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.orange.orangeserviceapi.common.exception.OrangeServiceApiBusinessException;
import com.orange.orangeserviceapi.common.exception.OrangeServiceApiDataNotFoundException;
import com.orange.orangeserviceapi.common.exception.OrangeServiceApiDataValidateException;
import com.orange.orangeserviceapi.common.exception.OrangeServiceApiException;
import com.orange.orangeserviceapi.common.utils.GenericResponse;
import com.orange.orangeserviceapi.common.utils.ResultCode;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@ControllerAdvice
public class GlobalExceptionHandler {
	
	@ExceptionHandler(OrangeServiceApiException.class)
    public ResponseEntity<GenericResponse> handlerGeneralException(OrangeServiceApiException e) {
        log.error("Remark : [{}]",e.getStatus().getRemark());
        GenericResponse response = new GenericResponse();
        response.setStatus(ResultCode.SYSTEM_ERROR);
        return new ResponseEntity<>(response, HttpStatus.SERVICE_UNAVAILABLE);
    }
	
	@ExceptionHandler
	 public ResponseEntity<GenericResponse> dataNotFoundException(OrangeServiceApiDataNotFoundException e){
		return new ResponseEntity<>(new GenericResponse(e.getStatus()),HttpStatus.NOT_FOUND);
	}
	
	@ExceptionHandler
	 public ResponseEntity<GenericResponse> dataValidateException(OrangeServiceApiDataValidateException e){
		return new ResponseEntity<>(new GenericResponse(e.getStatus()),HttpStatus.BAD_REQUEST);
	}
	 
	@ExceptionHandler
	 public ResponseEntity<GenericResponse> dataBusinessException(OrangeServiceApiBusinessException e){
		return new ResponseEntity<>(new GenericResponse(e.getStatus()),HttpStatus.BAD_REQUEST);
	}
}