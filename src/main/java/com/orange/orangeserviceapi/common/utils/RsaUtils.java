package com.orange.orangeserviceapi.common.utils;

import com.orange.orangeserviceapi.common.exception.OrangeServiceApiDataValidateException;
import com.orange.orangeserviceapi.common.exception.OrangeServiceApiException;
import io.micrometer.core.instrument.util.StringUtils;
import lombok.extern.slf4j.Slf4j;

import java.security.KeyFactory;
import java.security.Security;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import javax.crypto.Cipher;

@Slf4j
public class RsaUtils {
    private static String MARK_MOBILE_NUMBER="XXX-XXX-";
    private static String MARK_CITIZEN_ID ="XXXX-XXXX-XX";
    public static String PUBLIC_KEY = "30819f300d06092a864886f70d010101050003818d003081890281810095e4997e607731875faa8a06ef9835bb23f008613bd3993832537145339f876755192e9447584652f59f6f03157fe615b5d318f10c66716072e7b36aae9f5cd5c20045129b056efc6ff412f39433a4aa3b15330c68090dc44e72f9a36c7988ad325802118482c0556555dfcfa77139e84cefccbd496d3520cdf9b5f55157b6930203010001";
    private static String PRIVATE_KEY = "30820277020100300d06092a864886f70d0101010500048202613082025d0201000281810095e4997e607731875faa8a06ef9835bb23f008613bd3993832537145339f876755192e9447584652f59f6f03157fe615b5d318f10c66716072e7b36aae9f5cd5c20045129b056efc6ff412f39433a4aa3b15330c68090dc44e72f9a36c7988ad325802118482c0556555dfcfa77139e84cefccbd496d3520cdf9b5f55157b69302030100010281804e66b69e728885c0801a6c7dba3e8c042984f86d1b64cf24a4b61e6e0ad56b7671fd6ceea3ee9941f5358254439f95524fbb6db54f3e1a194fa0bfa89253cab33adc884aa714ef030c344ae0fbf982cac9b774e25b741ec654e76928ffbcdc051041eef71671886fdb1cd1d2712cbd4e9d566a2f2a128edfbb8451c7d79c2e79024100cc7bee0650f5d9b6cdecc8350e481b6370284bbfb900e38a17c3047279c6f49c3d640c311e43e849c12a1f85e6a0a0a1e8d68a4533425f78f31d2c18dc1ea6fd024100bba7d98f4520a5a6560b9211fcef99c390fe48eab6dadc901d4c30c8a8084315197e9ac7b8bdc40357109cc85c433933ca61c75c30f33a3080fb6e94318070cf02406494a0e3e276cb7c2b964874306e2f6a9e04741dfdf5fdf632f1ee0210fc57dd7225000a3904d601514e52eb1e21ec6137922fb9121997b47a80e2ebe779fc51024100b090229cb1ff20bd1bd2b13269998b44be04f3d89c09db63619d917d0ad513412406c25d4ece86425a46102e58d90b5656829f9077a232caf6611c4f253063730241008f5fafcfa3102a674aedeb26157502a520c27a68f9093c53bda5ee44f67cae5d27f77dc8a70677284ab272d044e4488ad559738553486f472d8c3844deb44418";

    static {
        Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
    }

    public static String decrypt(String encryptedTxt) throws OrangeServiceApiException {
        try {
            KeyFactory keyFactory = KeyFactory.getInstance("RSA", "BC");
            PKCS8EncodedKeySpec privateKeySpec = new PKCS8EncodedKeySpec(hex2Byte(PRIVATE_KEY));
            RSAPrivateKey privKey = (RSAPrivateKey) keyFactory.generatePrivate(privateKeySpec);
            Cipher cipher = Cipher.getInstance("RSA/None/PKCS1Padding", "BC");
            cipher.init(Cipher.DECRYPT_MODE, privKey);
            byte[] plainText = cipher.doFinal(hex2Byte(encryptedTxt));
            return new String(plainText, "UTF-8");
        } catch (Exception e) {
            throw new OrangeServiceApiException(ResultCode.SYSTEM_ERROR,"Decrypt exception");
        }
    }

    private static byte[] hex2Byte(String src_hex) {
        byte[] des_byte = new byte[src_hex.length() / 2];
        for (int i = 0, j = 0; i < src_hex.length(); i += 2, j++) {
            des_byte[j] = (byte) Integer.parseInt(src_hex.substring(i, i + 2), 16);
        }
        return des_byte;
    }

    public static String encrypt(String plainText) throws OrangeServiceApiException {
        try {
            byte[] input = plainText.getBytes("UTF-8");
            KeyFactory keyFactory = KeyFactory.getInstance("RSA", "BC");
            X509EncodedKeySpec publicKeySpec = new X509EncodedKeySpec(hex2Byte(PUBLIC_KEY));
            RSAPublicKey pubKey = (RSAPublicKey) keyFactory.generatePublic(publicKeySpec);
            Cipher cipher = Cipher.getInstance("RSA/None/PKCS1Padding", "BC");
            cipher.init(Cipher.ENCRYPT_MODE, pubKey);
            byte[] cipherText = cipher.doFinal(input);
            return byte2Hex(cipherText);
        } catch (Exception e) {
            throw new OrangeServiceApiException(ResultCode.SYSTEM_ERROR,"Encrypt exception");
        }
    }

    private static String byte2Hex(byte[] src_byte) {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < src_byte.length; i++) {
            String temp = Integer.toHexString((int) src_byte[i]);
            if (temp.length() > 2) {
                temp = temp.substring(temp.length() - 2, temp.length());
            } else if (temp.length() < 2) {
                temp = "0" + temp;
            }
            sb.append(temp);
        }
        return sb.toString().trim();
    }

    public static String markMobileNumber(String plainText) throws OrangeServiceApiException{
        if(StringUtils.isNotEmpty(plainText) && plainText.length() == 10){
            String result = MARK_MOBILE_NUMBER+plainText.substring(6,10);
            return result;
        }
        throw new OrangeServiceApiDataValidateException(ResultCode.INVALID_PARAM,"Phone number length must be equals 10 digit.");
    }

    public static String markCitizenId(String plainText)throws OrangeServiceApiException{
        if(StringUtils.isNotEmpty(plainText) && plainText.length() == 13){
            String result = MARK_CITIZEN_ID+plainText.substring(9,13);
            return result;
        }
        throw new OrangeServiceApiDataValidateException(ResultCode.INVALID_PARAM,"Citizen id length must be equals 13 digit.");
    }
}
