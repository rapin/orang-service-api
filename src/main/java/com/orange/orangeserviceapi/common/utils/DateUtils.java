package com.orange.orangeserviceapi.common.utils;

import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class DateUtils {
	private static Locale local_en = new Locale("en");

	public static Date getCurrentDate() {
		Calendar cal = Calendar.getInstance(local_en);
		return cal.getTime();
	}
}