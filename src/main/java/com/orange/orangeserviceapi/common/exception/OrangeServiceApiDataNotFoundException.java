package com.orange.orangeserviceapi.common.exception;

import com.orange.orangeserviceapi.common.utils.Status;

public class OrangeServiceApiDataNotFoundException extends OrangeServiceApiException {

	public OrangeServiceApiDataNotFoundException(Status status) {
		super(status);
	}
}