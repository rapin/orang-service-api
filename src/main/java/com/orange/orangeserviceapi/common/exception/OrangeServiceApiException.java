package com.orange.orangeserviceapi.common.exception;

import com.orange.orangeserviceapi.common.utils.Status;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OrangeServiceApiException extends Exception {
	private Status status;
	public OrangeServiceApiException(Status status) {
		this.status = status;
	}

	public OrangeServiceApiException(Status status,String repleteMessage) {
		super(repleteMessage);
		status.setRemark(repleteMessage);
		this.status = status;
	}
}