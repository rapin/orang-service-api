package com.orange.orangeserviceapi.common.exception;

import com.orange.orangeserviceapi.common.utils.Status;

public class OrangeServiceApiBusinessException extends OrangeServiceApiException {
	public OrangeServiceApiBusinessException(Status status, String repleteMessage) {
		super(status, repleteMessage);
	}
}