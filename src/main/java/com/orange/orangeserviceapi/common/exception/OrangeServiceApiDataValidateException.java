package com.orange.orangeserviceapi.common.exception;

import com.orange.orangeserviceapi.common.utils.Status;

public class OrangeServiceApiDataValidateException extends OrangeServiceApiException {

	public OrangeServiceApiDataValidateException(Status status, String repleteMessage) {
		super(status, repleteMessage);
	}
}