package com.orange.orangeserviceapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OrangeServiceApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(OrangeServiceApiApplication.class, args);
	}

}
