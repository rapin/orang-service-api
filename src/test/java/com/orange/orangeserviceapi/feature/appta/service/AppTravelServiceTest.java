package com.orange.orangeserviceapi.feature.appta.service;

import static org.mockito.ArgumentMatchers.anyString;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.TestComponent;

import com.orange.orangeserviceapi.common.exception.OrangeServiceApiDataNotFoundException;
import com.orange.orangeserviceapi.common.exception.OrangeServiceApiException;
import com.orange.orangeserviceapi.common.utils.GenericResponse;
import com.orange.orangeserviceapi.common.utils.ResultCode;
import com.orange.orangeserviceapi.datasource.entities.AppTravelDetail;
import com.orange.orangeserviceapi.datasource.entities.Application;
import com.orange.orangeserviceapi.datasource.entities.Beneficiary;
import com.orange.orangeserviceapi.datasource.entities.CustomerDetail;
import com.orange.orangeserviceapi.datasource.repo.AppTravelDetailRepo;
import com.orange.orangeserviceapi.datasource.repo.ApplicationRepo;
import com.orange.orangeserviceapi.datasource.repo.BeneficiaryRepo;
import com.orange.orangeserviceapi.datasource.repo.CustomerDetailRepo;
import com.orange.orangeserviceapi.feature.appta.controller.domian.BeneficiaryRequestController;
import com.orange.orangeserviceapi.feature.appta.controller.domian.CustomerDetailRequestController;
import com.orange.orangeserviceapi.feature.appta.controller.domian.TravelAppRequestController;

@TestComponent
public class AppTravelServiceTest {
	@InjectMocks private AppTravelService appTravelServiceMock;
	@Mock private ApplicationRepo applicationRepo;
	@Mock private CustomerDetailRepo customerDetailRepo;
	@Mock private AppTravelDetailRepo appTravelDetailRepo;
	@Mock private BeneficiaryRepo beneficiaryRepo;
	@Mock private ValidateTravelAppService validateTravelAppService;

	@Before
	public void beforeTest() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void saveTravelSuccess() throws OrangeServiceApiException {
		// Given
		TravelAppRequestController travelAppRequest = new TravelAppRequestController();
		travelAppRequest.setPlanCode("PlanCode");
		travelAppRequest.setArrivalDate(new Date());
		travelAppRequest.setDepartureDate(new Date());
		travelAppRequest.setLocation("BKK");

		CustomerDetailRequestController customerDetail = new CustomerDetailRequestController();
		customerDetail.setAddress("Address");
		customerDetail.setCitizenId("1234567890123");
		customerDetail.setEmailAddress("email@email.com");
		customerDetail.setLastName("LastName");
		customerDetail.setName("Name");
		customerDetail.setPassportNumber("PassportNumber");
		customerDetail.setPhoneNumber("0981234545");
		customerDetail.setTitle("Title");
		travelAppRequest.setCustomerDetail(customerDetail);

		List<BeneficiaryRequestController> beneficiaryList = new ArrayList<BeneficiaryRequestController>();
		BeneficiaryRequestController beneficiary = new BeneficiaryRequestController();
		beneficiary.setLastName("aaa");
		beneficiary.setName("bbb");
		beneficiary.setPercentage(100);
		beneficiary.setRelationship("fff");
		beneficiaryList.add(beneficiary);
		travelAppRequest.setBeneficiaryList(beneficiaryList);

		List<Beneficiary> beneficiaryListResult = new ArrayList<Beneficiary>();
		Beneficiary beneficiaryMock = new Beneficiary();
		beneficiaryListResult.add(beneficiaryMock);

		List<Beneficiary> beneficiaries = new ArrayList<>();
		Beneficiary validateBeneficiary = new Beneficiary();
		validateBeneficiary.setApplicationNumber("APP_NO");
		beneficiaries.add(validateBeneficiary);

		Application travelApp = new Application();
		travelApp.setApplicationNumber("APP_NO");
		Mockito.when(this.validateTravelAppService.validateSaveApplicationTravel(
				Mockito.any(TravelAppRequestController.class))).thenReturn(travelApp);

		CustomerDetail customerDetailMock = new CustomerDetail();
		Mockito.when(this.validateTravelAppService.validateSaveCustomerDetail(
				Mockito.any(CustomerDetailRequestController.class))).thenReturn(customerDetailMock);

		AppTravelDetail appTravelDetail = new AppTravelDetail();
		Mockito.when(this.validateTravelAppService.validateBeforeSaveAppTravelDetail(
				Mockito.any(TravelAppRequestController.class))).thenReturn(appTravelDetail);

		Mockito.when(this.validateTravelAppService.validateSaveBeneficiary(
				Mockito.any(TravelAppRequestController.class)
				,Mockito.anyString())).thenReturn(beneficiaries);

		Mockito.when(this.applicationRepo.saveAndFlush(Mockito.any(Application.class)))
				.thenReturn(travelApp);
		Mockito.when(this.customerDetailRepo.saveAndFlush(Mockito.any(CustomerDetail.class)))
				.thenReturn(new CustomerDetail());
		Mockito.when(this.appTravelDetailRepo.saveAndFlush(Mockito.any(AppTravelDetail.class)))
				.thenReturn(new AppTravelDetail());
		Mockito.when(this.beneficiaryRepo.saveAll(Mockito.anyList())).thenReturn(beneficiaryListResult);

		// When
		GenericResponse response = this.appTravelServiceMock.saveTravelApp(travelAppRequest);

		// Then
		Assert.assertEquals(ResultCode.SUCCESS.getCode(), response.getStatus().getCode());
	}

	@Test
	public void saveTravelSuccess_WithOutBeneficiary() throws OrangeServiceApiException {
		// Given
		TravelAppRequestController travelAppRequest = new TravelAppRequestController();
		travelAppRequest.setPlanCode("PlanCode");
		travelAppRequest.setArrivalDate(new Date());
		travelAppRequest.setDepartureDate(new Date());
		travelAppRequest.setLocation("BKK");

		CustomerDetailRequestController customerDetail = new CustomerDetailRequestController();
		customerDetail.setAddress("Address");
		customerDetail.setCitizenId("1234567890123");
		customerDetail.setEmailAddress("email@email.com");
		customerDetail.setLastName("LastName");
		customerDetail.setName("Name");
		customerDetail.setPassportNumber("PassportNumber");
		customerDetail.setPhoneNumber("0981234545");
		customerDetail.setTitle("Title");
		travelAppRequest.setCustomerDetail(customerDetail);

		List<BeneficiaryRequestController> beneficiaryList = new ArrayList<BeneficiaryRequestController>();
		BeneficiaryRequestController beneficiary = new BeneficiaryRequestController();
		beneficiary.setLastName("aaa");
		beneficiary.setName("bbb");
		beneficiary.setPercentage(100);
		beneficiary.setRelationship("fff");
		beneficiaryList.add(beneficiary);
		travelAppRequest.setBeneficiaryList(beneficiaryList);

		List<Beneficiary> beneficiaryListResult = new ArrayList<Beneficiary>();
		Beneficiary beneficiaryMock = new Beneficiary();
		beneficiaryListResult.add(beneficiaryMock);

		Application travelApp = new Application();
		Mockito.when(this.validateTravelAppService.validateSaveApplicationTravel(
				Mockito.any(TravelAppRequestController.class))).thenReturn(travelApp);

		CustomerDetail customerDetailMock = new CustomerDetail();
		Mockito.when(this.validateTravelAppService.validateSaveCustomerDetail(
				Mockito.any(CustomerDetailRequestController.class))).thenReturn(customerDetailMock);

		AppTravelDetail appTravelDetail = new AppTravelDetail();
		Mockito.when(this.validateTravelAppService.validateBeforeSaveAppTravelDetail(
				Mockito.any(TravelAppRequestController.class))).thenReturn(appTravelDetail);

		List<Beneficiary> beneficiaries = new ArrayList<>();
		Mockito.when(this.validateTravelAppService.validateSaveBeneficiary(Mockito.any(TravelAppRequestController.class)
				,Mockito.anyString())).thenReturn(beneficiaries);

		Mockito.when(this.applicationRepo.saveAndFlush(Mockito.any(Application.class)))
				.thenReturn(new Application());
		Mockito.when(this.customerDetailRepo.saveAndFlush(Mockito.any(CustomerDetail.class)))
				.thenReturn(new CustomerDetail());
		Mockito.when(this.appTravelDetailRepo.saveAndFlush(Mockito.any(AppTravelDetail.class)))
				.thenReturn(new AppTravelDetail());
		Mockito.when(this.beneficiaryRepo.saveAll(Mockito.anyList())).thenReturn(beneficiaryListResult);

		// When
		GenericResponse response = this.appTravelServiceMock.saveTravelApp(travelAppRequest);

		// Then
		Assert.assertEquals(ResultCode.SUCCESS.getCode(), response.getStatus().getCode());
	}

	@Test
	public void saveTravelSystemError_WhenSaveApplicationFail() throws OrangeServiceApiException{
		//Given
		TravelAppRequestController travelAppRequest = new TravelAppRequestController();
		travelAppRequest.setPlanCode("PlanCode");
		travelAppRequest.setArrivalDate(new Date());
		travelAppRequest.setDepartureDate(new Date());
		travelAppRequest.setLocation("BKK");
		Mockito.when(this.applicationRepo.saveAndFlush(Mockito.any(Application.class))).thenReturn(null);
		try {
			// When
			GenericResponse response = this.appTravelServiceMock.saveTravelApp(travelAppRequest);
			Assert.assertEquals(ResultCode.SYSTEM_ERROR.getCode(), response.getStatus().getCode());
		} catch (OrangeServiceApiException e) {
			// Then
			Assert.assertEquals(ResultCode.SYSTEM_ERROR.getCode(), e.getStatus().getCode());
		}
	}

	@Test
	public void saveTravelSystemError_WhenSaveCustomerDetailFail() throws OrangeServiceApiException {
		// Given
		TravelAppRequestController travelAppRequest = new TravelAppRequestController();
		travelAppRequest.setPlanCode("PlanCode");
		travelAppRequest.setArrivalDate(new Date());
		travelAppRequest.setDepartureDate(new Date());
		travelAppRequest.setLocation("BKK");

		CustomerDetailRequestController customerDetail = new CustomerDetailRequestController();
		customerDetail.setAddress("Address");
		customerDetail.setCitizenId("1234567890123");
		customerDetail.setEmailAddress("email@email.com");
		customerDetail.setLastName("LastName");
		customerDetail.setName("Name");
		customerDetail.setPassportNumber("PassportNumber");
		customerDetail.setPhoneNumber("0981234545");
		customerDetail.setTitle("Title");
		travelAppRequest.setCustomerDetail(customerDetail);

		Application travelApp = new Application();
		travelApp.setRowId("001");
		travelApp.setApplicationNumber("APP_NO");
		Mockito.when(this.validateTravelAppService.validateSaveApplicationTravel(
				Mockito.any(TravelAppRequestController.class))).thenReturn(travelApp);

		CustomerDetail customerDetailMock = new CustomerDetail();
		customerDetailMock.setApplicationNumber("APP_NO");
		Mockito.when(this.validateTravelAppService.validateSaveCustomerDetail(
				Mockito.any(CustomerDetailRequestController.class))).thenReturn(customerDetailMock);

		Mockito.when(this.applicationRepo.saveAndFlush(Mockito.any(Application.class)))
				.thenReturn(travelApp);

		Mockito.when(this.customerDetailRepo.saveAndFlush(Mockito.any(CustomerDetail.class)))
				.thenReturn(null);
		try {
			// When
			GenericResponse response = this.appTravelServiceMock.saveTravelApp(travelAppRequest);
			Assert.assertEquals(ResultCode.SYSTEM_ERROR.getCode(), response.getStatus().getCode());
		} catch (OrangeServiceApiException e) {
			// Then
			Assert.assertEquals(ResultCode.SYSTEM_ERROR.getCode(), e.getStatus().getCode());
		}
	}

	@Test
	public void saveTravelSystemError_WhenSaveAppTravelDetailFail() throws OrangeServiceApiException {
		// Given
		TravelAppRequestController travelAppRequest = new TravelAppRequestController();
		travelAppRequest.setPlanCode("PlanCode");
		travelAppRequest.setArrivalDate(new Date());
		travelAppRequest.setDepartureDate(new Date());
		travelAppRequest.setLocation("BKK");

		CustomerDetailRequestController customerDetail = new CustomerDetailRequestController();
		customerDetail.setAddress("Address");
		customerDetail.setCitizenId("1234567890123");
		customerDetail.setEmailAddress("email@email.com");
		customerDetail.setLastName("LastName");
		customerDetail.setName("Name");
		customerDetail.setPassportNumber("PassportNumber");
		customerDetail.setPhoneNumber("0981234545");
		customerDetail.setTitle("Title");
		travelAppRequest.setCustomerDetail(customerDetail);

		Application travelApp = new Application();
		travelApp.setRowId("001");
		travelApp.setApplicationNumber("APP_NO");
		Mockito.when(this.validateTravelAppService.validateSaveApplicationTravel(
				Mockito.any(TravelAppRequestController.class))).thenReturn(travelApp);

		CustomerDetail customerDetailMock = new CustomerDetail();
		customerDetailMock.setApplicationNumber("APP_NO");
		Mockito.when(this.validateTravelAppService.validateSaveCustomerDetail(
				Mockito.any(CustomerDetailRequestController.class))).thenReturn(customerDetailMock);


		Mockito.when(this.validateTravelAppService.validateBeforeSaveAppTravelDetail(
				Mockito.any(TravelAppRequestController.class))).thenReturn(new AppTravelDetail());

		Mockito.when(this.applicationRepo.saveAndFlush(Mockito.any(Application.class)))
				.thenReturn(travelApp);

		Mockito.when(this.customerDetailRepo.saveAndFlush(Mockito.any(CustomerDetail.class)))
				.thenReturn(new CustomerDetail());

		Mockito.when(this.appTravelDetailRepo.saveAndFlush(Mockito.any(AppTravelDetail.class)))
				.thenReturn(null);
		try {
			// When
			GenericResponse response = this.appTravelServiceMock.saveTravelApp(travelAppRequest);
			Assert.assertEquals(ResultCode.SYSTEM_ERROR.getCode(), response.getStatus().getCode());
		} catch (OrangeServiceApiException e) {
			// Then
			Assert.assertEquals(ResultCode.SYSTEM_ERROR.getCode(), e.getStatus().getCode());
		}
	}

	@Test
	public void saveTravelSystemError_WhenSaveBeneficiaryFail() throws OrangeServiceApiException {
		// Given
		TravelAppRequestController travelAppRequest = new TravelAppRequestController();
		travelAppRequest.setPlanCode("PlanCode");
		travelAppRequest.setArrivalDate(new Date());
		travelAppRequest.setDepartureDate(new Date());
		travelAppRequest.setLocation("BKK");

		CustomerDetailRequestController customerDetail = new CustomerDetailRequestController();
		customerDetail.setAddress("Address");
		customerDetail.setCitizenId("1234567890123");
		customerDetail.setEmailAddress("email@email.com");
		customerDetail.setLastName("LastName");
		customerDetail.setName("Name");
		customerDetail.setPassportNumber("PassportNumber");
		customerDetail.setPhoneNumber("0981234545");
		customerDetail.setTitle("Title");
		travelAppRequest.setCustomerDetail(customerDetail);

		List<BeneficiaryRequestController> beneficiaryList = new ArrayList<BeneficiaryRequestController>();
		BeneficiaryRequestController beneficiary = new BeneficiaryRequestController();
		beneficiary.setLastName("aaa");
		beneficiary.setName("bbb");
		beneficiary.setPercentage(100);
		beneficiary.setRelationship("fff");
		beneficiaryList.add(beneficiary);
		travelAppRequest.setBeneficiaryList(beneficiaryList);

		List<Beneficiary> beneficiaryListResult = new ArrayList<Beneficiary>();
		Beneficiary beneficiaryMock = new Beneficiary();
		beneficiaryListResult.add(beneficiaryMock);

		List<Beneficiary> beneficiaries = new ArrayList<>();
		Beneficiary validateBeneficiary = new Beneficiary();
		validateBeneficiary.setApplicationNumber("APP_NO");
		beneficiaries.add(validateBeneficiary);

		Application travelApp = new Application();
		travelApp.setApplicationNumber("APP_NO");
		Mockito.when(this.validateTravelAppService.validateSaveApplicationTravel(
				Mockito.any(TravelAppRequestController.class))).thenReturn(travelApp);

		CustomerDetail customerDetailMock = new CustomerDetail();
		Mockito.when(this.validateTravelAppService.validateSaveCustomerDetail(
				Mockito.any(CustomerDetailRequestController.class))).thenReturn(customerDetailMock);

		AppTravelDetail appTravelDetail = new AppTravelDetail();
		Mockito.when(this.validateTravelAppService.validateBeforeSaveAppTravelDetail(
				Mockito.any(TravelAppRequestController.class))).thenReturn(appTravelDetail);

		Mockito.when(this.validateTravelAppService.validateSaveBeneficiary(
				Mockito.any(TravelAppRequestController.class)
				,Mockito.anyString())).thenReturn(beneficiaries);

		Mockito.when(this.applicationRepo.saveAndFlush(Mockito.any(Application.class)))
				.thenReturn(travelApp);
		Mockito.when(this.customerDetailRepo.saveAndFlush(Mockito.any(CustomerDetail.class)))
				.thenReturn(new CustomerDetail());
		Mockito.when(this.appTravelDetailRepo.saveAndFlush(Mockito.any(AppTravelDetail.class)))
				.thenReturn(new AppTravelDetail());

		List<Beneficiary> beneficiaryResultList = new ArrayList<>();
		Mockito.when(this.beneficiaryRepo.saveAll(Mockito.anyList())).thenReturn(beneficiaryResultList);

		try {
			// When
			GenericResponse response = this.appTravelServiceMock.saveTravelApp(travelAppRequest);
			Assert.assertEquals(ResultCode.SYSTEM_ERROR.getCode(), response.getStatus().getCode());
		} catch (OrangeServiceApiException e) {
			// Then
			Assert.assertEquals(ResultCode.SYSTEM_ERROR.getCode(), e.getStatus().getCode());
		}
	}


    @Test
    public void searchAppNotFound() throws OrangeServiceApiException {
        //Given
    	Mockito.when(this.applicationRepo.findByApplicationNumber(anyString())).thenReturn(null);

        try {
            //When
            GenericResponse response = this.appTravelServiceMock.searchApplication("TA20200416101");
            Assert.assertEquals(ResultCode.DATA_NOT_FOUND.getCode(),response.getStatus().getCode());
        }catch (OrangeServiceApiDataNotFoundException e){
            //Then
            Assert.assertEquals(ResultCode.DATA_NOT_FOUND.getCode(),e.getStatus().getCode());
        }
    }

    @Test
    public void searchAppSuccess() throws OrangeServiceApiException {
        //Given
    	Application application = new Application();
    	Mockito.when(this.applicationRepo.findByApplicationNumber(anyString())).thenReturn(application);

        //When
        GenericResponse response = this.appTravelServiceMock.searchApplication("TA20200416101151");

        //Then
        Assert.assertEquals(ResultCode.SUCCESS.getCode(),response.getStatus().getCode());
    }
}