package com.orange.orangeserviceapi.feature.appta.controller;

import com.orange.orangeserviceapi.common.exception.OrangeServiceApiException;
import com.orange.orangeserviceapi.common.utils.GenericResponse;
import com.orange.orangeserviceapi.common.utils.ResultCode;
import com.orange.orangeserviceapi.feature.appta.controller.domian.TravelAppRequestController;
import com.orange.orangeserviceapi.feature.appta.service.AppTravelService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.TestComponent;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

@TestComponent
public class AppTravelControllerTest {
    @InjectMocks private AppTravelController appTravelControllerMock;
    @Mock private AppTravelService appTravelService;
    @Before
    public void setup()
    {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void saveTravelAppSuccess() throws OrangeServiceApiException {
        //Given
        TravelAppRequestController travelAppRequestController = new TravelAppRequestController();
        GenericResponse response  = new GenericResponse();
        response.setStatus(ResultCode.SUCCESS);
        Mockito.when(this.appTravelService.saveTravelApp(Mockito.any(TravelAppRequestController.class))).thenReturn(response);

        //When
        ResponseEntity<GenericResponse> result = this.appTravelControllerMock.saveTravelApp("CORR-0001", travelAppRequestController);

        //Then
        Assert.assertEquals(HttpStatus.CREATED, result.getStatusCode());
    }
    
    @Test
    public void searchApplicationSuccess() throws OrangeServiceApiException {
        //Given
        GenericResponse response  = new GenericResponse();
        response.setStatus(ResultCode.SUCCESS);
        Mockito.when(this.appTravelService.searchApplication(Mockito.any())).thenReturn(response);

        //When
        ResponseEntity<GenericResponse> result = this.appTravelControllerMock.searchApplication("CORR-0001", "TA2020041515180");

        //Then
        Assert.assertEquals(HttpStatus.OK, result.getStatusCode());
    }
}
