package com.orange.orangeserviceapi.feature.appta.service;

import com.orange.orangeserviceapi.common.exception.OrangeServiceApiDataValidateException;
import com.orange.orangeserviceapi.common.exception.OrangeServiceApiException;
import com.orange.orangeserviceapi.common.utils.ResultCode;
import com.orange.orangeserviceapi.datasource.entities.AppTravelDetail;
import com.orange.orangeserviceapi.datasource.entities.Application;
import com.orange.orangeserviceapi.datasource.entities.Beneficiary;
import com.orange.orangeserviceapi.datasource.entities.CustomerDetail;
import com.orange.orangeserviceapi.feature.appta.controller.domian.BeneficiaryRequestController;
import com.orange.orangeserviceapi.feature.appta.controller.domian.CustomerDetailRequestController;
import com.orange.orangeserviceapi.feature.appta.controller.domian.TravelAppRequestController;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.TestComponent;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@TestComponent
public class ValidateTravelAppServiceTest {
	@InjectMocks
	private ValidateTravelAppService validateTravelAppServiceMock;

	@Before
	public void beforeTest() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void validateSaveCustomerDetail_Success() throws OrangeServiceApiException{
		// Given
		CustomerDetailRequestController req = new CustomerDetailRequestController();
		req.setTitle("Title");
		req.setName("TestName");
		req.setLastName("TestLastName");
		req.setPhoneNumber("0854545454");
		req.setCitizenId("1234567890123");
		req.setPassportNumber("");
		req.setEmailAddress("test@gmail.com");
		req.setAddress("bangkok 10400");

		// When
		CustomerDetail response = this.validateTravelAppServiceMock.validateSaveCustomerDetail(req);

		// Then
		Assert.assertNotNull(response);
	}

	@Test
	public void validateSaveCustomerDetail_SuccessWithOutCitizenId() throws OrangeServiceApiException{
		// Given
		CustomerDetailRequestController req = new CustomerDetailRequestController();
		req.setTitle("Title");
		req.setName("TestName");
		req.setLastName("TestLastName");
		req.setPhoneNumber("0854545454");
		req.setCitizenId("");
		req.setPassportNumber("1234567890123");
		req.setEmailAddress("test@gmail.com");
		req.setAddress("bangkok 10400");

		// When
		CustomerDetail response = this.validateTravelAppServiceMock.validateSaveCustomerDetail(req);

		// Then
		Assert.assertNotNull(response);
		Assert.assertEquals(response.getCitizenId(),"");
		Assert.assertNull(response.getCitizenIdEncr());
		Assert.assertEquals(response.getPassportNumber(),req.getPassportNumber());

	}


	@Test
	public void validateSaveCustomerDetail_TitleIsEmpty() throws OrangeServiceApiException {
		// Given
		CustomerDetailRequestController req = new CustomerDetailRequestController();
		req.setTitle("");
		req.setName("TestName");
		req.setLastName("TestLastName");
		req.setPhoneNumber("0854545454");
		req.setCitizenId("184644646565656");
		req.setPassportNumber("");
		req.setEmailAddress("test@gmail.com");
		req.setAddress("bangkok 10400");

		try {
			// When
			CustomerDetail response = this.validateTravelAppServiceMock.validateSaveCustomerDetail(req);
			Assert.assertNull(response);
		} catch (OrangeServiceApiDataValidateException e) {
			// Then
			Assert.assertEquals(ResultCode.INVALID_PARAM.getCode(), e.getStatus().getCode());
		}
	}

	@Test
	public void validateSaveCustomerDetail_NameIsEmpty() throws OrangeServiceApiException {
		// Given
		CustomerDetailRequestController req = new CustomerDetailRequestController();
		req.setTitle("Mr.");
		req.setName("");
		req.setLastName("TestLastName");
		req.setPhoneNumber("0854545454");
		req.setCitizenId("184644646565656");
		req.setPassportNumber("");
		req.setEmailAddress("test@gmail.com");
		req.setAddress("bangkok 10400");

		try {
			// When
			CustomerDetail response = this.validateTravelAppServiceMock.validateSaveCustomerDetail(req);
			Assert.assertNull(response);
		} catch (OrangeServiceApiDataValidateException e) {
			// Then
			Assert.assertEquals(ResultCode.INVALID_PARAM.getCode(), e.getStatus().getCode());
		}
	}

	@Test
	public void validateSaveCustomerDetail_LastNameIsEmpty() throws OrangeServiceApiException {
		// Given
		CustomerDetailRequestController req = new CustomerDetailRequestController();
		req.setTitle("Mr.");
		req.setName("TestName");
		req.setLastName("");
		req.setPhoneNumber("0854545454");
		req.setCitizenId("184644646565656");
		req.setPassportNumber("");
		req.setEmailAddress("test@gmail.com");
		req.setAddress("bangkok 10400");

		try {
			// When
			CustomerDetail response = this.validateTravelAppServiceMock.validateSaveCustomerDetail(req);
			Assert.assertNull(response);
		} catch (OrangeServiceApiDataValidateException e) {
			// Then
			Assert.assertEquals(ResultCode.INVALID_PARAM.getCode(), e.getStatus().getCode());
		}
	}

	@Test
	public void validateSaveCustomerDetail_PhoneIsEmpty() throws OrangeServiceApiException {
		// Given
		CustomerDetailRequestController req = new CustomerDetailRequestController();
		req.setTitle("Mr.");
		req.setName("TestName");
		req.setLastName("TestLastName");
		req.setPhoneNumber("");
		req.setCitizenId("184644646565656");
		req.setPassportNumber("");
		req.setEmailAddress("test@gmail.com");
		req.setAddress("bangkok 10400");

		try {
			// When
			CustomerDetail response = this.validateTravelAppServiceMock.validateSaveCustomerDetail(req);
			Assert.assertNull(response);
		} catch (OrangeServiceApiDataValidateException e) {
			// Then
			Assert.assertEquals(ResultCode.INVALID_PARAM.getCode(), e.getStatus().getCode());
		}
	}

	@Test
	public void validateSaveCustomerDetail_PhoneOverLimitLength() throws OrangeServiceApiException {
		// Given
		CustomerDetailRequestController req = new CustomerDetailRequestController();
		req.setTitle("Mr.");
		req.setName("TestName");
		req.setLastName("TestLastName");
		req.setPhoneNumber("085555555555555555");
		req.setCitizenId("184644646565656");
		req.setPassportNumber("");
		req.setEmailAddress("test@gmail.com");
		req.setAddress("bangkok 10400");

		try {
			// When
			CustomerDetail response = this.validateTravelAppServiceMock.validateSaveCustomerDetail(req);
			Assert.assertNull(response);
		} catch (OrangeServiceApiDataValidateException e) {
			// Then
			Assert.assertEquals(ResultCode.INVALID_PARAM.getCode(), e.getStatus().getCode());
		}
	}

	@Test
	public void validateSaveCustomerDetail_CitizenAndPassportIsEmpty() throws OrangeServiceApiException {
		// Given
		CustomerDetailRequestController req = new CustomerDetailRequestController();
		req.setTitle("Mr.");
		req.setName("TestName");
		req.setLastName("TestLastName");
		req.setPhoneNumber("0854545454");
		req.setCitizenId("");
		req.setPassportNumber("");
		req.setEmailAddress("test@gmail.com");
		req.setAddress("bangkok 10400");

		try {
			// When
			CustomerDetail response = this.validateTravelAppServiceMock.validateSaveCustomerDetail(req);
			Assert.assertNull(response);
		} catch (OrangeServiceApiDataValidateException e) {
			// Then
			Assert.assertEquals(ResultCode.INVALID_PARAM.getCode(), e.getStatus().getCode());
		}
	}

	@Test
	public void validateSaveCustomerDetail_EmailIsEmpty() throws OrangeServiceApiException {
		// Given
		CustomerDetailRequestController req = new CustomerDetailRequestController();
		req.setTitle("Mr.");
		req.setName("TestName");
		req.setLastName("TestLastName");
		req.setPhoneNumber("0854545454");
		req.setCitizenId("18959595959595");
		req.setEmailAddress("");
		req.setAddress("bangkok 10400");

		try {
			// When
			CustomerDetail response = this.validateTravelAppServiceMock.validateSaveCustomerDetail(req);
			Assert.assertNull(response);
		} catch (OrangeServiceApiDataValidateException e) {
			// Then
			Assert.assertEquals(ResultCode.INVALID_PARAM.getCode(), e.getStatus().getCode());
		}
	}

	@Test
	public void validateSaveCustomerDetail_EmailInvalidFormat() throws OrangeServiceApiException {
		// Given
		CustomerDetailRequestController req = new CustomerDetailRequestController();
		req.setTitle("Mr.");
		req.setName("TestName");
		req.setLastName("TestLastName");
		req.setPhoneNumber("0854545454");
		req.setCitizenId("18959595959595");
		req.setEmailAddress("test@1245@gmail");
		req.setAddress("bangkok 10400");

		try {
			// When
			CustomerDetail response = this.validateTravelAppServiceMock.validateSaveCustomerDetail(req);
			Assert.assertNull(response);
		} catch (OrangeServiceApiDataValidateException e) {
			// Then
			Assert.assertEquals(ResultCode.INVALID_PARAM.getCode(), e.getStatus().getCode());
		}
	}

	@Test
	public void validateSaveCustomerDetail_AddressIsEmpty() throws OrangeServiceApiException {
		// Given
		CustomerDetailRequestController req = new CustomerDetailRequestController();
		req.setTitle("Mr.");
		req.setName("TestName");
		req.setLastName("TestLastName");
		req.setPhoneNumber("0854545454");
		req.setCitizenId("");
		req.setPassportNumber("18959595959595");
		req.setEmailAddress("test@gmail.com");
		req.setAddress("");

		try {
			// When
			CustomerDetail response = this.validateTravelAppServiceMock.validateSaveCustomerDetail(req);
			Assert.assertNull(response);
		} catch (OrangeServiceApiDataValidateException e) {
			// Then
			Assert.assertEquals(ResultCode.INVALID_PARAM.getCode(), e.getStatus().getCode());
		}
	}

	@Test
	public void validateBeforeSaveAppTravelDetailInvalidParam_WhenArrivalDateIsNull() throws OrangeServiceApiException {
		// Given
		TravelAppRequestController travelAppRequest = new TravelAppRequestController();
		travelAppRequest.setArrivalDate(null);
		travelAppRequest.setDepartureDate(new Date());
		travelAppRequest.setLocation("BKK");
		try {
			// When
			AppTravelDetail response = this.validateTravelAppServiceMock.validateBeforeSaveAppTravelDetail(travelAppRequest);
			Assert.assertNull(response);
		} catch (OrangeServiceApiException e) {
			// Then
			Assert.assertEquals(ResultCode.INVALID_PARAM.getCode(), e.getStatus().getCode());
		}
	}

	@Test
	public void validateBeforeSaveAppTravelDetailInvalidParam_WhenDepartureDateIsNull()
			throws OrangeServiceApiException {
		// Given
		TravelAppRequestController travelAppRequest = new TravelAppRequestController();
		travelAppRequest.setArrivalDate(new Date());
		travelAppRequest.setDepartureDate(null);
		travelAppRequest.setLocation("BKK");
		try {
			// When
			AppTravelDetail response = this.validateTravelAppServiceMock.validateBeforeSaveAppTravelDetail(travelAppRequest);
			Assert.assertNull(response);
		} catch (OrangeServiceApiException e) {
			// Then
			Assert.assertEquals(ResultCode.INVALID_PARAM.getCode(), e.getStatus().getCode());
		}
	}

	@Test
	public void validateBeforeSaveAppTravelDetailInvalidParam_WhenLocationIsEmpty() throws OrangeServiceApiException {
		// Given
		TravelAppRequestController travelAppRequest = new TravelAppRequestController();
		travelAppRequest.setArrivalDate(new Date());
		travelAppRequest.setDepartureDate(new Date());
		travelAppRequest.setLocation("");
		try {
			// When
			AppTravelDetail response = this.validateTravelAppServiceMock.validateBeforeSaveAppTravelDetail(travelAppRequest);
			Assert.assertNull(response);
		} catch (OrangeServiceApiException e) {
			// Then
			Assert.assertEquals(ResultCode.INVALID_PARAM.getCode(), e.getStatus().getCode());
		}
	}

	@Test
	public void validateSaveBeneficiary_Success() throws OrangeServiceApiException {
		// Given
		TravelAppRequestController travelAppRequest = new TravelAppRequestController();
		travelAppRequest.setPlanCode("PlanCode");
		travelAppRequest.setArrivalDate(new Date());
		travelAppRequest.setDepartureDate(new Date());
		travelAppRequest.setLocation("BKK");

		CustomerDetailRequestController customerDetail = new CustomerDetailRequestController();
		customerDetail.setLastName("LastName");
		customerDetail.setName("Name");
		travelAppRequest.setCustomerDetail(customerDetail);

		List<BeneficiaryRequestController> beneficiaryList = new ArrayList<BeneficiaryRequestController>();
		BeneficiaryRequestController beneficiary = new BeneficiaryRequestController();
		beneficiary.setLastName("aaa");
		beneficiary.setName("bbb");
		beneficiary.setPercentage(100);
		beneficiary.setRelationship("fff");
		beneficiaryList.add(beneficiary);
		travelAppRequest.setBeneficiaryList(beneficiaryList);

		//When
		List<Beneficiary> response = this.validateTravelAppServiceMock.validateSaveBeneficiary(travelAppRequest,"APP_NUMBER");

		//Then
		Assert.assertNotEquals(response.size(),0);
	}

	@Test
	public void validateSaveBeneficiary_InvalidParamNameIsEmpty() {
		// Given
		TravelAppRequestController travelAppRequest = new TravelAppRequestController();
		travelAppRequest.setPlanCode("PlanCode");
		travelAppRequest.setArrivalDate(new Date());
		travelAppRequest.setDepartureDate(new Date());
		travelAppRequest.setLocation("BKK");

		CustomerDetailRequestController customerDetail = new CustomerDetailRequestController();
		customerDetail.setLastName("LastName");
		customerDetail.setName("Name");
		travelAppRequest.setCustomerDetail(customerDetail);

		List<BeneficiaryRequestController> beneficiaryList = new ArrayList<BeneficiaryRequestController>();
		BeneficiaryRequestController beneficiary = new BeneficiaryRequestController();
		beneficiary.setName("");
		beneficiaryList.add(beneficiary);
		travelAppRequest.setBeneficiaryList(beneficiaryList);
		try {
			//When
			List<Beneficiary> response = this.validateTravelAppServiceMock.validateSaveBeneficiary(travelAppRequest, "APP_NUMBER");
			Assert.assertNotEquals(response.size(),0);
		} catch (OrangeServiceApiException e) {
			//Then
			Assert.assertEquals(ResultCode.INVALID_PARAM.getCode(), e.getStatus().getCode());
		}
	}

	@Test
	public void validateSaveBeneficiary_InvalidParamLastNameIsEmpty() {
		// Given
		TravelAppRequestController travelAppRequest = new TravelAppRequestController();
		travelAppRequest.setPlanCode("PlanCode");
		travelAppRequest.setArrivalDate(new Date());
		travelAppRequest.setDepartureDate(new Date());
		travelAppRequest.setLocation("BKK");

		CustomerDetailRequestController customerDetail = new CustomerDetailRequestController();
		customerDetail.setName("Name");
		customerDetail.setLastName("LastName");
		travelAppRequest.setCustomerDetail(customerDetail);

		List<BeneficiaryRequestController> beneficiaryList = new ArrayList<BeneficiaryRequestController>();
		BeneficiaryRequestController beneficiary = new BeneficiaryRequestController();
		beneficiary.setName("aaa");
		beneficiary.setLastName("");
		beneficiaryList.add(beneficiary);
		travelAppRequest.setBeneficiaryList(beneficiaryList);
		try {
			//When
			List<Beneficiary> response = this.validateTravelAppServiceMock.validateSaveBeneficiary(travelAppRequest, "APP_NUMBER");
			Assert.assertNotEquals(response.size(),0);
		} catch (OrangeServiceApiException e) {
			//Then
			Assert.assertEquals(ResultCode.INVALID_PARAM.getCode(), e.getStatus().getCode());
		}
	}

	@Test
	public void validateSaveBeneficiary_InvalidParamPercentageLessThanZeroOrEqualsZero() {
		// Given
		TravelAppRequestController travelAppRequest = new TravelAppRequestController();
		travelAppRequest.setPlanCode("PlanCode");
		travelAppRequest.setArrivalDate(new Date());
		travelAppRequest.setDepartureDate(new Date());
		travelAppRequest.setLocation("BKK");

		CustomerDetailRequestController customerDetail = new CustomerDetailRequestController();
		customerDetail.setLastName("LastName");
		customerDetail.setName("Name");
		travelAppRequest.setCustomerDetail(customerDetail);

		List<BeneficiaryRequestController> beneficiaryList = new ArrayList<BeneficiaryRequestController>();
		BeneficiaryRequestController beneficiary = new BeneficiaryRequestController();
		beneficiary.setLastName("aaa");
		beneficiary.setName("bbb");
		beneficiary.setRelationship("Relationship");
		beneficiary.setPercentage(0);
		beneficiaryList.add(beneficiary);
		travelAppRequest.setBeneficiaryList(beneficiaryList);
		try {
			//When
			List<Beneficiary> response = this.validateTravelAppServiceMock.validateSaveBeneficiary(travelAppRequest, "APP_NUMBER");
			Assert.assertNotEquals(response.size(),0);
		} catch (OrangeServiceApiException e) {
			//Then
			Assert.assertEquals(ResultCode.INVALID_PARAM.getCode(), e.getStatus().getCode());
		}
	}

	@Test
	public void validateSaveBeneficiary_InvalidParamRelationShipIsEmpty() {
		// Given
		TravelAppRequestController travelAppRequest = new TravelAppRequestController();
		travelAppRequest.setPlanCode("PlanCode");
		travelAppRequest.setArrivalDate(new Date());
		travelAppRequest.setDepartureDate(new Date());
		travelAppRequest.setLocation("BKK");

		CustomerDetailRequestController customerDetail = new CustomerDetailRequestController();
		customerDetail.setLastName("LastName");
		customerDetail.setName("Name");
		travelAppRequest.setCustomerDetail(customerDetail);

		List<BeneficiaryRequestController> beneficiaryList = new ArrayList<BeneficiaryRequestController>();
		BeneficiaryRequestController beneficiary = new BeneficiaryRequestController();
		beneficiary.setLastName("aaa");
		beneficiary.setName("bbb");
		beneficiary.setRelationship("");
		beneficiary.setPercentage(50);
		beneficiaryList.add(beneficiary);
		travelAppRequest.setBeneficiaryList(beneficiaryList);
		try {
			//When
			List<Beneficiary> response = this.validateTravelAppServiceMock.validateSaveBeneficiary(travelAppRequest, "APP_NUMBER");
			Assert.assertNotEquals(response.size(),0);
		} catch (OrangeServiceApiException e) {
			//Then
			Assert.assertEquals(ResultCode.INVALID_PARAM.getCode(), e.getStatus().getCode());
		}
	}

	@Test
	public void validateSaveBeneficiary_BeneficiaryName_Invalid() {
		// Given
		TravelAppRequestController travelAppRequest = new TravelAppRequestController();
		travelAppRequest.setPlanCode("PlanCode");
		travelAppRequest.setArrivalDate(new Date());
		travelAppRequest.setDepartureDate(new Date());
		travelAppRequest.setLocation("BKK");

		CustomerDetailRequestController customerDetail = new CustomerDetailRequestController();
		customerDetail.setLastName("aaa");
		customerDetail.setName("bbb");
		travelAppRequest.setCustomerDetail(customerDetail);

		List<BeneficiaryRequestController> beneficiaryList = new ArrayList<BeneficiaryRequestController>();
		BeneficiaryRequestController beneficiary = new BeneficiaryRequestController();
		beneficiary.setLastName("aaa");
		beneficiary.setName("bbb");
		beneficiary.setPercentage(50);
		beneficiary.setRelationship("Relationship");
		beneficiaryList.add(beneficiary);
		travelAppRequest.setBeneficiaryList(beneficiaryList);
		try {
			//When
			List<Beneficiary> response = this.validateTravelAppServiceMock.validateSaveBeneficiary(travelAppRequest, "APP_NUMBER");
			Assert.assertNotEquals(response.size(),0);
		} catch (OrangeServiceApiException e) {
			//Then
			Assert.assertEquals(ResultCode.INVALID_BUSINESS.getCode(), e.getStatus().getCode());
		}
	}

	@Test
	public  void validateSaveApplicationTravel_Success()throws OrangeServiceApiException{
		//Given
		TravelAppRequestController travelAppRequest = new TravelAppRequestController();
		travelAppRequest.setPlanCode("PLAN_CODE");

		//When
		Application application = this.validateTravelAppServiceMock.validateSaveApplicationTravel(travelAppRequest);

		//Then
		Assert.assertNotNull(application);
	}

	@Test
	public  void validateSaveApplicationTravel_WhenPlanCodeLengthMoreThanTen()throws OrangeServiceApiException{
		//Given
		TravelAppRequestController travelAppRequest = new TravelAppRequestController();
		travelAppRequest.setPlanCode("PLAN_CODE-0001");
		try{
			//When
			Application application = this.validateTravelAppServiceMock.validateSaveApplicationTravel(travelAppRequest);
			Assert.assertNull(application);
		}catch (OrangeServiceApiDataValidateException e){
			//Then
			Assert.assertEquals(e.getStatus().getCode(),ResultCode.INVALID_PARAM.getCode());
		}
	}

	@Test
	public  void validateSaveApplicationTravel_WhenPlanCodeIsEmpty()throws OrangeServiceApiException{
		//Given
		TravelAppRequestController travelAppRequest = new TravelAppRequestController();
		travelAppRequest.setPlanCode("");
		try{
			//When
			Application application = this.validateTravelAppServiceMock.validateSaveApplicationTravel(travelAppRequest);
			Assert.assertNull(application);
		}catch (OrangeServiceApiDataValidateException e){
			//Then
			Assert.assertEquals(e.getStatus().getCode(),ResultCode.INVALID_PARAM.getCode());
		}
	}

	@Test
	public void validateBeforeSaveAppTravelDetail() throws OrangeServiceApiException {
		//Given
		TravelAppRequestController travelAppRequest = new TravelAppRequestController();
		travelAppRequest.setDepartureDate(new Date());
		travelAppRequest.setArrivalDate(new Date());
		travelAppRequest.setLocation("Location");
		//When
		AppTravelDetail appTravelDetail = this.validateTravelAppServiceMock.validateBeforeSaveAppTravelDetail(travelAppRequest);

		//Then
		Assert.assertNotNull(appTravelDetail);
	}

	@Test
	public void validateBeforeSaveAppTravelDetail_WhenDepartureDateIsNull() throws OrangeServiceApiException {
		//Given
		TravelAppRequestController travelAppRequest = new TravelAppRequestController();
		try{
			//When
			AppTravelDetail appTravelDetail = this.validateTravelAppServiceMock.validateBeforeSaveAppTravelDetail(travelAppRequest);
			Assert.assertNull(appTravelDetail);
		}catch (OrangeServiceApiDataValidateException e){
			//Then
			Assert.assertEquals(e.getStatus().getCode(),ResultCode.INVALID_PARAM.getCode());
		}
	}

	@Test
	public void validateBeforeSaveAppTravelDetail_WhenArrivalDateIsNull() throws OrangeServiceApiException {
		//Given
		TravelAppRequestController travelAppRequest = new TravelAppRequestController();
		travelAppRequest.setDepartureDate(new Date());
		try{
			//When
			AppTravelDetail appTravelDetail = this.validateTravelAppServiceMock.validateBeforeSaveAppTravelDetail(travelAppRequest);
			Assert.assertNull(appTravelDetail);
		}catch (OrangeServiceApiDataValidateException e){
			//Then
			Assert.assertEquals(e.getStatus().getCode(),ResultCode.INVALID_PARAM.getCode());
		}
	}

	@Test
	public void validateBeforeSaveAppTravelDetail_WhenLocationIsEmpty() throws OrangeServiceApiException {
		//Given
		TravelAppRequestController travelAppRequest = new TravelAppRequestController();
		travelAppRequest.setDepartureDate(new Date());
		travelAppRequest.setArrivalDate(new Date());
		travelAppRequest.setLocation("");
		try{
			//When
			AppTravelDetail appTravelDetail = this.validateTravelAppServiceMock.validateBeforeSaveAppTravelDetail(travelAppRequest);
			Assert.assertNull(appTravelDetail);
		}catch (OrangeServiceApiDataValidateException e){
			//Then
			Assert.assertEquals(e.getStatus().getCode(),ResultCode.INVALID_PARAM.getCode());
		}
	}

	@Test
	public void validateSaveBeneficiary() throws OrangeServiceApiException {
		TravelAppRequestController beneficiaryRequest = new TravelAppRequestController();
		List<BeneficiaryRequestController> beneficiaryListRequest = new ArrayList<>();
		beneficiaryRequest.setBeneficiaryList(beneficiaryListRequest);

		//When
		List<Beneficiary> beneficiaryList = this.validateTravelAppServiceMock.validateSaveBeneficiary(beneficiaryRequest,
				"applicationNumber");

		//Then
		Assert.assertEquals(beneficiaryList.size(),0);
	}
}
