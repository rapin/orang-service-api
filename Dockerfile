# load official base image of Java Runtim
FROM openjdk:8-jdk-alpine

# Set volume point to /tmp
VOLUME /tmp

# Make port 8890 available to the world outside container
EXPOSE 8890

ARG JAR_FILE=target/orange-service-api-0.0.1-04.jar

# Add jar file to the container
ADD ${JAR_FILE} app.jar

# Start application
ENTRYPOINT ["java", "-jar", "/app.jar"]